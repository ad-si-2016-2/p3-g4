var express = require('express');
var irc = require('./irc/ircParser.js');
var http = require('http');
var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var socketio = require('socket.io');
var path = require('path');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'static')));

var proxy_id = 0;
var server = http.createServer(app);
var io = socketio(server);

app.get('/', function (req, res) {
  if (req.cookies.servidor && req.cookies.nick && req.cookies.canal) {
    res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
    res.sendFile(path.join(__dirname, '/login.html'));
  }
});

app.post('/login', function (req, res) {
  res.cookie('nick', req.body.nome);
  res.cookie('canal', req.body.canal);
  res.cookie('servidor', req.body.servidor);
  res.redirect('/');
});

server.listen(3000, function () {
  console.log('Client IRC rodando em localhost: ' + 3000);
});

io.on('connection', function (socket) {
  irc.newConnection(socket);
});